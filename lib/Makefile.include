##
## This file is part of the libopencm3 project.
##
## Copyright (C) 2009 Uwe Hermann <uwe@hermann-uwe.de>
## Copyright (C) 2012 Piotr Esden-Tempski <piotr@esden.net>
##
## This library is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with this library.  If not, see <http://www.gnu.org/licenses/>.
##

# Be silent per default, but 'make V=1' will show all compiler calls.
ifneq ($(V),1)
Q := @
endif

TOPDIR := $(SRCLIBDIR)/..

# common objects
OBJS += vector.o systick.o scb.o nvic.o assert.o sync.o dwt.o

# Slightly bigger .elf files but gains the ability to decode macros
DEBUG_FLAGS ?= -ggdb3
STANDARD_FLAGS ?= -std=c99

all: $(SRCLIBDIR)/$(LIBNAME).a

GENFILES = \
	$(TOPDIR)/include/libopencm3/$(TARGET)/nvic.h \
	$(TOPDIR)/include/libopencmsis/$(TARGET)/irqhandlers.h \
	$(TOPDIR)/lib/$(TARGET)/vector_nvic.c

IRQ_DEFN = $(TOPDIR)/include/libopencm3/$(TARGET)/irq.json

$(GENFILES) : $(IRQ_DEFN)
	@printf "  GENHDR   $(TARGET) %s\n" '$<';
	$(Q)cd $(TOPDIR) && ./scripts/irq2nvic_h ./$(subst $(TOPDIR)/,,$<)

# Real dependencies come in via *.d files
$(OBJS): | $(if $(wildcard $(IRQ_DEFN)), $(GENFILES),)

$(SRCLIBDIR)/$(LIBNAME).a: $(OBJS)
	@printf "  AR      $(LIBNAME).a\n"
	$(Q)$(AR) $(ARFLAGS) "$@" $(OBJS)

%.o: %.c
	@printf "  CC      $(<F)\n"
	$(Q)$(CC) $(TGT_CFLAGS) $(CFLAGS) -o $@ -c $<

clean:
	$(Q)rm -f *.o *.d ../*.o ../*.d
	$(Q)rm -f $(SRCLIBDIR)/$(LIBNAME).a

.PHONY: clean

-include $(OBJS:.o=.d)
